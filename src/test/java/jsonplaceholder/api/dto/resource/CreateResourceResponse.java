package jsonplaceholder.api.dto.resource;

import com.frameworkium.core.api.dto.AbstractDTO;

public class CreateResourceResponse extends AbstractDTO<CreateResourceResponse> {
    public Resource resource;
}
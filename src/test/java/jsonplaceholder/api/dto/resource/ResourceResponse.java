package jsonplaceholder.api.dto.resource;

import com.frameworkium.core.api.dto.AbstractDTO;

public class ResourceResponse extends AbstractDTO<ResourceResponse> {
    public Resource resource;
}

